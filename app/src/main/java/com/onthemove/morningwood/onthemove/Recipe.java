package com.onthemove.morningwood.onthemove;

import android.graphics.drawable.Drawable;
import android.view.View;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class Recipe {
    public static int SWEET = 1 << 0;
    public static int SOUR = 1 << 1;
    public static int SPICY = 1 << 2;
    public static int SAVORY = 1 << 3;
    public static int SALTY = 1 << 4;

    public static int OVERNIGHT = 1 << 0;
    public static int SUNDAY_BAKING = 1 << 1;
    public static int MORNING = 1 << 2;

    public static int ALL = SWEET | SOUR | SPICY | SAVORY | SALTY | OVERNIGHT | SUNDAY_BAKING | MORNING;

    private String name;
    private Drawable image;
    private int difficulty;
    private int tasteProfile;
    private int bakingProfile;
    private int filter;

    private Ingredient[] ingredients;
    private String[] instructions;

    public View itemView;

    public Recipe(String name, String imageURL, int difficulty, int tasteProfile, int bakingProfile, String[] instructions, Ingredient... ingredients) {
        this.name = name;
        try {
            this.image = new ImageLoader().execute(imageURL).get();
        } catch (ExecutionException | InterruptedException e) {e.printStackTrace();}

        this.difficulty = difficulty;
        this.tasteProfile = tasteProfile;
        this.ingredients = ingredients;
        this.bakingProfile = bakingProfile;
        this.instructions = instructions;
    }

    public String getName() {
        return name;
    }

    public Drawable getImage() {
        return image;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public int getTasteProfile() {
        return tasteProfile;
    }

    public boolean isSour() {
        return (tasteProfile & SOUR) == SOUR;
    }

    public boolean isSweet() {
        return (tasteProfile & SWEET) == SWEET;
    }

    public boolean isSpicy() {
        return (tasteProfile & SPICY) == SPICY;
    }

    public boolean isSavory() {
        return (tasteProfile & SAVORY) == SAVORY;
    }

    public boolean isSalty() {
        return (tasteProfile & SALTY) == SALTY;
    }

    public Ingredient[] getIngredients() {
        return ingredients;
    }

    public Ingredient getIngredient(int index) {
        return ingredients[index];
    }

    public boolean matchesFilter(int filter) {
        this.filter = tasteProfile | bakingProfile << 5;

        if ((filter & ALL) == ALL) return true;

        for (int i = 0; i < 16; i++) {
            if (((this.filter >> i) & 0b1) == ((filter >> i) & 0b1) && ((this.filter >> i) & 0b1) != 0) return true;
        }

        return false;
    }

    public String[] getInstructions() {
        return instructions;
    }

    public String getInstruction(int index) {
        return instructions[index];
    }
}
