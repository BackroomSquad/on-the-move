package com.onthemove.morningwood.onthemove;

import android.graphics.drawable.Drawable;
import android.util.Log;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class RecipeManager {

    public static List<Recipe> toRecipe(List<List<Object>> data) {
        List<Recipe> recipes = new ArrayList<Recipe>();

        int maxRowLength = 0;
        for (List<Object> list : data)
            if (list.size() > maxRowLength) maxRowLength = list.size();

        for (int col = 0; col < maxRowLength; col += 4) {
            int fase = 0;

            String name = null;
            String imageURL = null;
            List<Ingredient> ingredients = new ArrayList<Ingredient>();

            int difficulty = 0;
            int tasteProfile = 0;
            int bakingProfile = 0;

            List<String> instructions = new ArrayList<String>();

            for (int row = 0; row < data.size(); row++) {
                List<Object> cols = data.get(row);
                if (cols.size() <= col) continue;

                String value = (String) cols.get(col);

                if (value.isEmpty()) continue;

                if (value.equals("-")) {
                    fase++;
                    continue;
                }

                if (fase == 0) {
                    if (row == 0) name = value;
                    else if (row == 1) imageURL = value;
                    else {
                        ingredients.add(new Ingredient((String) cols.get(col+2), value, (String) cols.get(col+1)));
                    }
                } else if (fase == 1) {
                    String tasteProfileString = (String) cols.get(col+1);
                    String bakingProfileString = (String) cols.get(col+2);

                    tasteProfile = (tasteProfileString.contains("SW") ? Recipe.SWEET : 0) | (tasteProfileString.contains("SO") ? Recipe.SOUR : 0) | (tasteProfileString.contains("SP") ? Recipe.SPICY : 0) | (tasteProfileString.contains("SAV") ? Recipe.SAVORY : 0) | (tasteProfileString.contains("SAL") ? Recipe.SALTY : 0);
                    bakingProfile = (bakingProfileString.contains("ON") ? Recipe.OVERNIGHT : 0) | (bakingProfileString.contains("SB") ? Recipe.SUNDAY_BAKING : 0) | (bakingProfileString.contains("MO") ? Recipe.MORNING : 0);
                    difficulty = Integer.parseInt(value);
                } else if (fase == 2) {
                    instructions.add(value);
                }
            }

            String[] instructionsArray = new String[instructions.size()];
            instructionsArray = instructions.toArray(instructionsArray);

            Ingredient[] ingredientsArray = new Ingredient[ingredients.size()];
            ingredientsArray = ingredients.toArray(ingredientsArray);

            recipes.add(new Recipe(name, imageURL, difficulty, tasteProfile, bakingProfile, instructionsArray, ingredientsArray));
        }

        return recipes;
    }
}
