package com.onthemove.morningwood.onthemove;

import java.util.ArrayList;
import java.util.List;

public class RecipeHandler {
    private List<Recipe> recipes = new ArrayList<Recipe>();
    private List<Recipe> currentRecipes = new ArrayList<Recipe>();

    private int currentFilter = Recipe.ALL;

    public void addRecipe(Recipe r) {
        recipes.add(r);
    }

    public void addRecipes(List<Recipe> recipes) {
        for (Recipe recipe : recipes) this.recipes.add(recipe);
    }

    public List<Recipe> getRecipe() {
        updateCurrentRecipe();

        return currentRecipes;
    }

    private void updateCurrentRecipe() {
        currentRecipes.clear();

        for (Recipe r : recipes) {
            if (r.matchesFilter(currentFilter)) currentRecipes.add(r);
        }
    }

    public void setCurrentFilter(int filter) {
        currentFilter = filter;
    }
}
