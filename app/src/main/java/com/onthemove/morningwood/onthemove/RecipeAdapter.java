package com.onthemove.morningwood.onthemove;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.mortbay.jetty.Main;

import java.util.List;

public class RecipeAdapter extends BaseAdapter {
    Context context;
    List<Recipe> recipes;
    LayoutInflater inflter;

    public RecipeAdapter(Context applicationContext, List<Recipe> recipes) {
        this.context = context;
        this.recipes = recipes;
        inflter = (LayoutInflater.from(applicationContext));
    }

    @Override
    public int getCount() {
        return recipes.size();
    }

    @Override
    public Object getItem(int i) {
        return recipes.get(i).itemView;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.recipe_list, null);
        TextView name = (TextView) view.findViewById(R.id.name);
        ImageView food = (ImageView) view.findViewById(R.id.foodImage);
        ImageView[] stars = new ImageView[] {(ImageView) view.findViewById(R.id.star1), (ImageView) view.findViewById(R.id.star2), (ImageView) view.findViewById(R.id.star3)};
        ImageView[] profiles = new ImageView[] {(ImageView) view.findViewById(R.id.sweet), (ImageView) view.findViewById(R.id.sour), (ImageView) view.findViewById(R.id.spicy), (ImageView) view.findViewById(R.id.savory), (ImageView) view.findViewById(R.id.salty)};
        name.setText(recipes.get(i).getName());
        food.setImageDrawable(recipes.get(i).getImage());

        if (recipes.get(i).getDifficulty() == 2) {
            stars[2].setVisibility(View.INVISIBLE);
        } else if (recipes.get(i).getDifficulty() == 1) {
            stars[2].setVisibility(View.INVISIBLE);
            stars[1].setVisibility(View.INVISIBLE);
        }

        if ((recipes.get(i).getTasteProfile() & Recipe.SWEET) == 0) profiles[0].setVisibility(View.INVISIBLE);
        if ((recipes.get(i).getTasteProfile() & Recipe.SOUR) == 0) profiles[1].setVisibility(View.INVISIBLE);
        if ((recipes.get(i).getTasteProfile() & Recipe.SPICY) == 0) profiles[2].setVisibility(View.INVISIBLE);
        if ((recipes.get(i).getTasteProfile() & Recipe.SAVORY) == 0) profiles[3].setVisibility(View.INVISIBLE);
        if ((recipes.get(i).getTasteProfile() & Recipe.SALTY) == 0) profiles[4].setVisibility(View.INVISIBLE);

        recipes.get(i).itemView = view;

        return view;
    }

    public void setRecipes(List<Recipe> recipes) {
        this.recipes.clear();

        this.recipes = recipes;
    }

    public boolean isEnabled(int position) {
        return MainActivity.currentRecipe == null;
    }
}
