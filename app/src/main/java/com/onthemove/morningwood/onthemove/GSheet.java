package com.onthemove.morningwood.onthemove;
import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.sheets.v4.Sheets;
import com.google.api.services.sheets.v4.SheetsScopes;
import com.google.api.services.sheets.v4.model.ValueRange;
import com.google.api.client.http.HttpTransport;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import pub.devrel.easypermissions.AfterPermissionGranted;
import pub.devrel.easypermissions.EasyPermissions;

public class GSheet extends AsyncTask<Void, Void, List<List<Object>>> {
    private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

    private static final String[] SCOPES = {SheetsScopes.SPREADSHEETS_READONLY};

    private static Activity context;
    public static List<List<Object>> data;
    GoogleAccountCredential credential;
    RecipeHandler rh;

    public GSheet(Activity context, RecipeHandler rh) {
    	this.context = context;
        credential = GoogleAccountCredential.usingOAuth2(context, Arrays.asList(SCOPES)).setBackOff(new ExponentialBackOff());
        this.rh = rh;
	}

    @AfterPermissionGranted(1003)
	public void tryToLoadData() {
        if (getStatus() == Status.FINISHED || getStatus() == Status.RUNNING) return;

        if (EasyPermissions.hasPermissions(context, Manifest.permission.GET_ACCOUNTS)) {
            String accountName = context.getPreferences(Context.MODE_PRIVATE).getString("accountName", null);
            Log.d("Account Name", accountName==null?"Null":accountName);

            if (accountName == null) {
                context.startActivityForResult(credential.newChooseAccountIntent(), 1000);
            } else {
                credential.setSelectedAccountName(accountName);
                execute();
            }
        } else {
            EasyPermissions.requestPermissions(context, "This app needs to access your Google account (via Contacts).", 1003, Manifest.permission.GET_ACCOUNTS);
        }
    }

    public List<List<Object>> getDataFromApi() {
    	List<List<Object>> result = null;

		try {
			// Build a new authorized API client service.
			final HttpTransport HTTP_TRANSPORT = AndroidHttp.newCompatibleTransport();
			final String spreadsheetId = "18gLng4iW0yNqEitK-kcsM6AxwNVbdLCPK5KPAKyabHs";
			final String range = "Opskrifter!A:ZZ";

			Sheets service = new Sheets.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)/*getCredentials(HTTP_TRANSPORT)*/
					.setApplicationName(context.getResources().getString(R.string.app_name))
					.build();
			ValueRange response = service.spreadsheets().values()
					.get(spreadsheetId, range)
					.execute();
			List<List<Object>> values = response.getValues();
			if (values == null || values.isEmpty()) Log.d("GSheet","No data found.");

			result = values;
		} catch (UserRecoverableAuthIOException e) {
            context.startActivityForResult(e.getIntent(), 1004);
        } catch (IOException e) {e.printStackTrace();}

        return result;
    }

    @Override
    protected List<List<Object>> doInBackground(Void... params) {
    	return getDataFromApi();
	}

	@Override
	protected void onPreExecute() {
    }

    @Override
    protected void onPostExecute(List<List<Object>> strings) {
        data = strings;
        rh.addRecipes(RecipeManager.toRecipe(strings));
        MainActivity.updateList(0b1111111111111111);
    }

    @Override
    protected void onCancelled() {
    }
}