package com.onthemove.morningwood.onthemove;

public class Ingredient {
    private String name;
    private String amount = null;
    private String unit;
    private String flavorText;

    public Ingredient(String name, String amount, String unit) {
        this.name = name;
        this.amount = amount;
        this.unit = unit;
    }

    public Ingredient(String name, double amount, String unit) {
        this(name, (amount == (int) amount) ? ((int) amount)+"" : amount+"", unit);
    }

    public Ingredient(String name, String flavorText) {
        this.name = name;
        this.flavorText = flavorText;
    }

    public String getName() {
        return name;
    }

    public String getAmount() {
        return amount;
    }

    public String getUnit() {
        return unit;
    }

    public String getFlavorText() {
        return flavorText;
    }

    @Override
    public String toString() {
        if (flavorText == null)
            return getAmount() + " " + getUnit() + " " + getName().toLowerCase();
        else
            return getFlavorText().substring(0, 1).toUpperCase() + getFlavorText().substring(1, getFlavorText().length()) + " " + getName(). toLowerCase();
    }
}
