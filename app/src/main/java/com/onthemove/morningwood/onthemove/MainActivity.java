package com.onthemove.morningwood.onthemove;

import android.accounts.AccountManager;
import android.animation.LayoutTransition;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.drawable.BitmapDrawable;
import android.support.constraint.ConstraintLayout;
import android.support.constraint.ConstraintSet;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import org.mortbay.jetty.security.Constraint;

import java.util.ArrayList;
import java.util.List;
import java.util.zip.Inflater;

public class MainActivity extends AppCompatActivity {

    private static ListView recipesList;
    private Spinner filterSpinner;
    private static RecipeHandler rh;
    private ViewGroup recipeContainer;
    private Context context;
    public static View currentRecipe;
    private View currentListRecipe;
    private GSheet sheetLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        recipeContainer = (ViewGroup) findViewById(R.id.recipeContainer);

        context = this;

        rh = new RecipeHandler();

        sheetLoader = new GSheet(this, rh);
        sheetLoader.tryToLoadData();

        //Initializing Recipes

        //Setting up list
        recipesList = (ListView) findViewById(R.id.recipes);
        RecipeAdapter recipeAdapterList = new RecipeAdapter(this, rh.getRecipe());
        recipesList.setAdapter(recipeAdapterList);
        recipesList.setOnItemClickListener(listClick);

        //Setting up list
        filterSpinner = (Spinner) findViewById(R.id.filter);
        ArrayAdapter<CharSequence> filterAdapterList = new ArrayAdapter<CharSequence>(this, R.layout.filter_spinner, R.id.elementName);
        filterAdapterList.addAll(getResources().getStringArray(R.array.filter_array));
        filterSpinner.setAdapter(filterAdapterList);
        filterSpinner.setOnItemSelectedListener(filterClick);
    }

    public static void updateList(int filter) {
        rh.setCurrentFilter(filter);
        ((RecipeAdapter) recipesList.getAdapter()).setRecipes(rh.getRecipe());

        Log.d("Recipes", rh.getRecipe().size()+"");

        ((RecipeAdapter) recipesList.getAdapter()).notifyDataSetChanged();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (android.os.Build.VERSION.SDK_INT > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            Log.d("CDA", "onKeyDown Called");
            onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        ObjectAnimator animatorSpinnerTranslation = ObjectAnimator.ofFloat(filterSpinner, "translationY", 0);
        animatorSpinnerTranslation.setDuration(1000);
        animatorSpinnerTranslation.start();

        ObjectAnimator animatorRecipeTranslation = ObjectAnimator.ofFloat(currentRecipe, "translationY", currentListRecipe.getTop()+((View) currentListRecipe.getParent()).getTop());
        animatorRecipeTranslation.setDuration(1000);
        animatorRecipeTranslation.start();

        ValueAnimator animatorViewScaleY = ValueAnimator.ofInt(currentRecipe.findViewById(R.id.foodImage).getLayoutParams().height, (int) (currentRecipe.findViewById(R.id.foodImage).getLayoutParams().height/(4.0/3.0)));
        animatorViewScaleY.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = currentRecipe.findViewById(R.id.foodImage).getLayoutParams();
                layoutParams.height = val;
                currentRecipe.findViewById(R.id.foodImage).setLayoutParams(layoutParams);
            }
        });
        ValueAnimator animatorViewScaleX = ValueAnimator.ofInt(currentRecipe.findViewById(R.id.foodImage).getLayoutParams().width, (int) (currentRecipe.findViewById(R.id.foodImage).getLayoutParams().width/(4.0/3.0)));
        animatorViewScaleX.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = currentRecipe.findViewById(R.id.foodImage).getLayoutParams();
                layoutParams.width = val;
                currentRecipe.findViewById(R.id.foodImage).setLayoutParams(layoutParams);
            }
        });
        animatorViewScaleX.setDuration(1000);
        animatorViewScaleY.setDuration(1000);
        animatorViewScaleX.start();
        animatorViewScaleY.start();


        ValueAnimator animatorViewTextSize = ValueAnimator.ofFloat(((TextView) currentRecipe.findViewById(R.id.name)).getTextSize(), ((TextView) currentRecipe.findViewById(R.id.name)).getTextSize()/2+3);
        animatorViewTextSize.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float val = (Float) valueAnimator.getAnimatedValue();
                ((TextView) currentRecipe.findViewById(R.id.name)).setTextSize(TypedValue.COMPLEX_UNIT_PX, val);
            }
        });
        animatorViewTextSize.setDuration(1000);
        animatorViewTextSize.start();

        ValueAnimator animatorViewRecipeScroll = ValueAnimator.ofInt(currentRecipe.findViewById(R.id.recipeScroll).getLayoutParams().height, 0);
        animatorViewRecipeScroll.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                int val = (Integer) valueAnimator.getAnimatedValue();
                ViewGroup.LayoutParams layoutParams = currentRecipe.findViewById(R.id.recipeScroll).getLayoutParams();
                layoutParams.height = val;
                currentRecipe.findViewById(R.id.recipeScroll).setLayoutParams(layoutParams);
            }
        });
        animatorViewRecipeScroll.setDuration(1000);
        animatorViewRecipeScroll.start();

        for (int i = 0; i  < rh.getRecipe().size(); i++) {
            View currentView = recipesList.getChildAt(i);
            ObjectAnimator animatorCurrentView = ObjectAnimator.ofFloat(currentView, "translationY", 0);
            animatorCurrentView.setDuration(1000);
            animatorCurrentView.start();
        }

        Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    Thread.sleep(1050);
                } catch (InterruptedException e) {
                }

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        recipeContainer.removeView(currentRecipe);
                        currentListRecipe.setVisibility(View.VISIBLE);

                        currentRecipe = null;
                    }
                });
            }
        };
        thread.start();
    }

    private void addRecipe(ViewGroup viewGroup, View clone, Recipe currentRecipe) {
        //Creates a new linear layout and sets it's position to the position and size of the clone view
        RelativeLayout recipe = new RelativeLayout(context);
        recipe.setLayoutParams(new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        recipe.setY(clone.getTop()+((View) clone.getParent()).getTop());

        //Creates a new image, sets it's image to the cloned views image and adds it to the recipe view
        ImageView recipeImage = new ImageView(context);
        recipeImage.setId(R.id.foodImage);
        recipeImage.setImageBitmap(((BitmapDrawable) ((ImageView) clone.findViewById(R.id.foodImage)).getDrawable()).getBitmap());
        ViewGroup.LayoutParams imageParam = new LinearLayout.LayoutParams(clone.findViewById(R.id.foodImage).getWidth(), clone.findViewById(R.id.foodImage).getHeight());
        recipeImage.setLayoutParams(imageParam);
        recipeImage.setPivotX(1);
        recipeImage.setPivotY(1);
        recipe.addView(recipeImage);

        //Creates a new text, sets it's text and it's parameters
        TextView recipeName = new TextView(context);
        recipeName.setId(R.id.name);
        recipeName.setText(((TextView) clone.findViewById(R.id.name)).getText());
        RelativeLayout.LayoutParams recipeNameLp = (RelativeLayout.LayoutParams) clone.findViewById(R.id.name).getLayoutParams();
        recipeNameLp.addRule(RelativeLayout.RIGHT_OF, recipeImage.getId());
        recipeName.setLayoutParams(recipeNameLp);
        recipe.addView(recipeName);

        //Creates a LinearLayout to contain all the stars
        LinearLayout recipeStarLayout = new LinearLayout(context);
        recipeStarLayout.setWeightSum(1);
        recipeStarLayout.setId(R.id.recipeStarLayout);
        RelativeLayout.LayoutParams recipeStarLayoutLp = (RelativeLayout.LayoutParams) clone.findViewById(R.id.recipeStarLayout).getLayoutParams();
        recipeStarLayoutLp.addRule(RelativeLayout.BELOW, recipeName.getId());
        recipeStarLayoutLp.addRule(RelativeLayout.RIGHT_OF, recipeImage.getId());
        recipeStarLayoutLp.leftMargin = 0;
        recipeStarLayout.setLayoutParams(recipeStarLayoutLp);
        recipe.addView(recipeStarLayout);

        //Creates a list of Images and makes them look like stars
        ImageView[] recipeStars = new ImageView[3];
        recipeStars[0] = new ImageView(context);
        recipeStars[1] = new ImageView(context);
        recipeStars[2] = new ImageView(context);
        recipeStars[0].setId(R.id.star1);
        recipeStars[1].setId(R.id.star2);
        recipeStars[2].setId(R.id.star3);
        recipeStars[0].setVisibility(clone.findViewById(R.id.star1).getVisibility());
        recipeStars[1].setVisibility(clone.findViewById(R.id.star2).getVisibility());
        recipeStars[2].setVisibility(clone.findViewById(R.id.star3).getVisibility());
        for (ImageView star : recipeStars) {
            star.setImageResource(R.drawable.star);
            LinearLayout.LayoutParams starLp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            starLp.weight = 0.33f;
            star.setLayoutParams(starLp);
            recipeStarLayout.addView(star);
        }

        //Creates a LinearLayout to contain all the profiles
        LinearLayout recipeProfileLayout = new LinearLayout(context);
        recipeProfileLayout.setWeightSum(1);
        RelativeLayout.LayoutParams recipeProfileLayoutLp = (RelativeLayout.LayoutParams) clone.findViewById(R.id.recipeTasteProfile).getLayoutParams();
        recipeProfileLayoutLp.addRule(RelativeLayout.BELOW, recipeName.getId());
        recipeProfileLayoutLp.addRule(RelativeLayout.RIGHT_OF, recipeStarLayout.getId());
        recipeProfileLayout.setLayoutParams(recipeProfileLayoutLp);
        recipe.addView(recipeProfileLayout);

        //Creates a list of Images and makes them look like stars
        ImageView[] recipeProfiles = new ImageView[5];
        recipeProfiles[0] = new ImageView(context);
        recipeProfiles[1] = new ImageView(context);
        recipeProfiles[2] = new ImageView(context);
        recipeProfiles[3] = new ImageView(context);
        recipeProfiles[4] = new ImageView(context);
        recipeProfiles[0].setId(R.id.sweet);
        recipeProfiles[1].setId(R.id.sour);
        recipeProfiles[2].setId(R.id.spicy);
        recipeProfiles[2].setId(R.id.savory);
        recipeProfiles[2].setId(R.id.salty);
        recipeProfiles[0].setVisibility(clone.findViewById(R.id.sweet).getVisibility());
        recipeProfiles[1].setVisibility(clone.findViewById(R.id.sour).getVisibility());
        recipeProfiles[2].setVisibility(clone.findViewById(R.id.spicy).getVisibility());
        recipeProfiles[3].setVisibility(clone.findViewById(R.id.savory).getVisibility());
        recipeProfiles[4].setVisibility(clone.findViewById(R.id.salty).getVisibility());
        recipeProfiles[0].setImageResource(R.drawable.sweet);
        recipeProfiles[1].setImageResource(R.drawable.sour);
        recipeProfiles[2].setImageResource(R.drawable.spicy);
        recipeProfiles[3].setImageResource(R.drawable.savory);
        recipeProfiles[4].setImageResource(R.drawable.salty);
        for (ImageView profile : recipeProfiles) {
            LinearLayout.LayoutParams profileLp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            profileLp.weight = 0.2f;
            profile.setLayoutParams(profileLp);
            recipeProfileLayout.addView(profile);
        }

        //Creates a ScrollView that contains all the ingredients and instructions
        ScrollView recipeScroll = new ScrollView(context);
        recipeScroll.setId(R.id.recipeScroll);
        RelativeLayout.LayoutParams recipeScrollLp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        recipeScrollLp.addRule(RelativeLayout.BELOW, recipeImage.getId());
        recipeScrollLp.leftMargin = 20;
        recipeScroll.setLayoutParams(recipeScrollLp);
        recipe.addView(recipeScroll);

        //Add container to ScrollView
        RelativeLayout recipeInstructionConstraint = new RelativeLayout(context);
        FrameLayout.LayoutParams recipeInstructionConstraintLp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        recipeInstructionConstraint.setLayoutParams(recipeInstructionConstraintLp);
        recipeScroll.addView(recipeInstructionConstraint);

        //----------------------------
        //Ingredient text starts here!
        //----------------------------

        //Adds ingredients TextViews
        TextView ingredientsHeader = new TextView(context);
        ingredientsHeader.setId(R.id.ingredientsHeader);
        ingredientsHeader.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 25);
        ingredientsHeader.setText(R.string.ingredientsText);
        RelativeLayout.LayoutParams ingredientsHeaderLp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ingredientsHeader.setLayoutParams(ingredientsHeaderLp);
        recipeInstructionConstraint.addView(ingredientsHeader);

        //Goes through all ingredients and adds it's name to a string of ingredients
        String ingr = "";
        for (int i = 0; i < currentRecipe.getIngredients().length; i++) {
            Ingredient ingredient = currentRecipe.getIngredient(i);

            ingr += ingredient.toString() + "\n";
        }

        TextView ingredients = new TextView(context);
        ingredients.setId(R.id.ingredients);
        ingredients.setTextSize(TypedValue.COMPLEX_UNIT_DIP,15);
        ingredients.setText(ingr);
        ingredients.setPadding(50,20,0,0);
        ingredients.setLineSpacing(0, 1.75f);
        RelativeLayout.LayoutParams ingredientsLp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        ingredientsLp.addRule(RelativeLayout.BELOW, ingredientsHeader.getId());
        ingredients.setLayoutParams(ingredientsLp);
        recipeInstructionConstraint.addView(ingredients);


        //Adds Instruction TextViews
        TextView instructionsHeader = new TextView(context);
        instructionsHeader.setId(R.id.instructionsHeader);
        instructionsHeader.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 25);
        instructionsHeader.setText(R.string.instructionsText);
        instructionsHeader.setPadding(0,50, 0, 0);
        RelativeLayout.LayoutParams instructionsHeaderLp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        instructionsHeaderLp.addRule(RelativeLayout.BELOW, ingredients.getId());
        instructionsHeader.setLayoutParams(instructionsHeaderLp);
        recipeInstructionConstraint.addView(instructionsHeader);

        //Goes through all instruction and adds it's name to a string of instructions
        String instr = "";
        for (int i = 0; i < currentRecipe.getInstructions().length; i++) {
            String instruction = currentRecipe.getInstruction(i) + "\n" + "\n";

            instr += instruction;
        }

        TextView instructions = new TextView(context);
        instructions.setId(R.id.instructions);
        instructions.setTextSize(TypedValue.COMPLEX_UNIT_DIP,15);
        instructions.setText(instr);
        instructions.setPadding(50,20,0,0);
        RelativeLayout.LayoutParams instructionsLp = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        instructionsLp.addRule(RelativeLayout.BELOW, instructionsHeader.getId());
        instructions.setLayoutParams(instructionsLp);
        recipeInstructionConstraint.addView(instructions);

        //Sets the final things up
        this.currentRecipe = recipe;
        viewGroup.addView(recipe);
    }

    public static AdapterView.OnItemSelectedListener filterClick = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position == 0)
                MainActivity.updateList(Recipe.ALL);
            else
                MainActivity.updateList(1 << (position-1));
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    public AdapterView.OnItemClickListener listClick = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            //Makes a clone of view and adds it to recipe container
            addRecipe(recipeContainer, view, rh.getRecipe().get(position));

            //Makes list element to invisible, so the clone can do the animation
            view.setVisibility(View.INVISIBLE);
            currentListRecipe = view;

            //Sets up the animation so the filter spinner can move off screen and the clone can got to Y=0
            ObjectAnimator animatorViewTranslation = ObjectAnimator.ofFloat(currentRecipe, "translationY", 0);
            ValueAnimator animatorViewScaleY = ValueAnimator.ofInt(currentRecipe.findViewById(R.id.foodImage).getLayoutParams().height, (int) (currentRecipe.findViewById(R.id.foodImage).getLayoutParams().height*(4.0/3.0)));
            animatorViewScaleY.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    int val = (Integer) valueAnimator.getAnimatedValue();
                    ViewGroup.LayoutParams layoutParams = currentRecipe.findViewById(R.id.foodImage).getLayoutParams();
                    layoutParams.height = val;
                    currentRecipe.findViewById(R.id.foodImage).setLayoutParams(layoutParams);
                }
            });
            ValueAnimator animatorViewScaleX = ValueAnimator.ofInt(currentRecipe.findViewById(R.id.foodImage).getLayoutParams().width, (int) (currentRecipe.findViewById(R.id.foodImage).getLayoutParams().width*(4.0/3.0)));
            animatorViewScaleX.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    int val = (Integer) valueAnimator.getAnimatedValue();
                    ViewGroup.LayoutParams layoutParams = currentRecipe.findViewById(R.id.foodImage).getLayoutParams();
                    layoutParams.width = val;
                    currentRecipe.findViewById(R.id.foodImage).setLayoutParams(layoutParams);
                }
            });
            ValueAnimator animatorViewTextSize = ValueAnimator.ofFloat(((TextView) currentRecipe.findViewById(R.id.name)).getTextSize()+3, ((TextView) currentRecipe.findViewById(R.id.name)).getTextSize()*2);
            animatorViewTextSize.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    float val = (Float) valueAnimator.getAnimatedValue();
                    ((TextView) currentRecipe.findViewById(R.id.name)).setTextSize(TypedValue.COMPLEX_UNIT_PX, val);
                }
            });
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            int height = displayMetrics.heightPixels;
            ValueAnimator animatorViewRecipeScroll = ValueAnimator.ofInt(0, ((ViewGroup) view.getParent()).getBottom()*2);
            animatorViewRecipeScroll.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator valueAnimator) {
                    int val = (Integer) valueAnimator.getAnimatedValue();
                    ViewGroup.LayoutParams layoutParams = currentRecipe.findViewById(R.id.recipeScroll).getLayoutParams();
                    layoutParams.height = val;
                    currentRecipe.findViewById(R.id.recipeScroll).setLayoutParams(layoutParams);
                }
            });
            ObjectAnimator animatorSpinnerTranslation = ObjectAnimator.ofFloat(filterSpinner, "translationY", -((ViewGroup) view.getParent()).getBottom()*2);

            //The animation should take one second
            animatorViewTranslation.setDuration(1000);
            animatorSpinnerTranslation.setDuration(1000);
            animatorViewScaleY.setDuration(1000);
            animatorViewScaleX.setDuration(1000);
            animatorViewTextSize.setDuration(1000);
            animatorViewRecipeScroll.setDuration(1000);

            //Start the animation
            animatorViewTranslation.start();
            animatorSpinnerTranslation.start();
            animatorViewScaleY.start();
            animatorViewScaleX.start();
            animatorViewTextSize.start();
            animatorViewRecipeScroll.start();

            //Goes through a list of all recipes
            for (int i = 0; i  < rh.getRecipe().size(); i++) {
                View currentView = (View) recipesList.getAdapter().getItem(i);
                ObjectAnimator animatorCurrentView;

                //If the element's position is less than the clicked elements position, then move it up else move it down
                if (i < position) {
                    animatorCurrentView = ObjectAnimator.ofFloat(currentView, "translationY", -((ViewGroup) view.getParent()).getBottom()*2);
                } else if (i > position) {
                    animatorCurrentView = ObjectAnimator.ofFloat(currentView, "translationY", ((ViewGroup) view.getParent()).getBottom()*2);
                } else continue;

                //Start all animation
                animatorCurrentView.setDuration(1000);
                animatorCurrentView.start();
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode) {
            case 1000:
                if (resultCode == RESULT_OK && data != null && data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        SharedPreferences settings =
                                getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString("accountName", accountName);
                        editor.apply();
                        sheetLoader.credential.setSelectedAccountName(accountName);
                        sheetLoader.tryToLoadData();
                    }
                }
                break;
        }
    }

    /*static int requestCode;
    public static boolean checkPermission(String permission, Context context) {
        boolean gotPermission = true;

        if (ContextCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permission)) {
                gotPermission = false;
                Log.w("Permission", "Didn't get permission: " + permission);
            } else {
                ActivityCompat.requestPermissions((Activity) context, new String[] {permission}, requestCode++);
            }
        }

        return gotPermission;
    }*/
}
